FROM alpine

ENV syncscriptsrc http://webdav/apps/parkade-displays-sync.sh

RUN apk update
RUN apk add samba
RUN apk add curl

ADD $syncscriptsrc /parkade-displays-sync.sh
RUN chmod a+x /parkade-displays-sync.sh

WORKDIR /
CMD ["/parkade-displays-sync.sh"]
